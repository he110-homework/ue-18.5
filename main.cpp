#include <iostream>
#include "Stack.h"

using namespace std;

int main() {
    Stack stack;

    stack.push(10);
    stack.push(500);
    stack.push(100500);
    stack.push(5);

    while(stack.length()) {
        cout << stack.pop() << endl;
    }
}
