#ifndef HOMEWORK_18_5_STACK_H
#define HOMEWORK_18_5_STACK_H

class Stack {
private:
    int* stack;
    int count = 0;

public:
    Stack(): stack(new int[0])
    {}

    void push(int item)
    {
        stack--;
        *stack = item;
        count++;
    }

    int pop()
    {
        count--;
        return *stack++;
    }

    int length() const
    {
        return count;
    }
};

#endif //HOMEWORK_18_5_STACK_H
